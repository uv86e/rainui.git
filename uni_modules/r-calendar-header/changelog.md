## 1.0.5（2025-02-17）
fix: 纯享版
## 1.0.4（2025-02-17）
修改参数
## 1.0.3（2024-08-13）
修复按月切换禁用的显示bug
## 1.0.2（2024-08-06）
更新文档
## 1.0.1（2024-08-01）
更新文档
## 1.0.0（2024-08-01）
first
