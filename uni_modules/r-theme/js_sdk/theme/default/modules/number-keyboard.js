import { computed } from "vue";

const numberKeyboardData = computed(() => ({
  "r-number-keyboard-background": "var(--r-gray-2)",
  "r-number-keyboard-key-height": "96rpx",
  "r-number-keyboard-key-font-size": "56rpx",
  "r-number-keyboard-key-active-color": "var(--r-gray-3)",
  "r-number-keyboard-key-background": "var(--r-background-2)",
  "r-number-keyboard-delete-font-size": "var(--r-font-size-lg)",
  "r-number-keyboard-title-color": "var(--r-gray-7)",
  "r-number-keyboard-title-height": "68rpx",
  "r-number-keyboard-title-font-size": "var(--r-font-size-lg)",
  "r-number-keyboard-close-padding": "0 var(--r-padding-md)",
  "r-number-keyboard-close-color": "var(--r-primary-color)",
  "r-number-keyboard-close-font-size": "var(--r-font-size-md)",
  "r-number-keyboard-button-text-color": "var(--r-white)",
  "r-number-keyboard-button-background": "var(--r-primary-color)",
  "r-number-keyboard-z-index": 100,
}));

export default numberKeyboardData;
