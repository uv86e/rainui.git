import { computed } from "vue";

const numberKeyboardData = computed(() => ({
  "r-number-keyboard-background": "var(--r-gray-8)",
  "r-number-keyboard-key-background": "var(--r-background-7)",
  "r-number-keyboard-key-active-color": "var(--r-gray-6)",
}));

export default numberKeyboardData;
